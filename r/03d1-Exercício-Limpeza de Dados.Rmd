---
title: Limpeza de Dados - Exercícios
output: html_document
---


Resolva os exercícios propostos abaixo. Os exercícios usam um dataset de reclamações registradas por cidadãos. O detaset foi derivado de dados do [Portal de Dados Abertos da Prefeitura de Curitiba](https://www.curitiba.pr.gov.br/DADOSABERTOS/).

## Leitura e análise inicial dos dados

Inicie um DataFrame a partir do arquivo `2017-02-01_156_-_Base_de_Dados_sample.csv`. Exiba algumas linhas e informações sobre os tipos identificados automaticamente pelo R.

**Dica:** Caso o arquivo não seja separado por vírgulas, o R não conseguirá reconhecer os campos adequadamente. Você precisará fornecer o parâmetro `sep=';'` para indicar o separador correto (neste caso, o `;`).

**Dica:** Aplicativos modernos tendem a armazenar arquivos no formato UTF-8, mas é comum encontrar arquivos codificados em outros formatos. Caso você tenha problemas para ler o arquivo, utilize o parâmetro `encoding='latin1'` para selecionar o encoding correto (*latin1* neste exemplo). Use também o parâmetro `stringsAsFactors = FALSE` para evitar conversões desnecessárias (detalhes sobre isso nos próximos tutoriais). Também é importante usar `na.strings=""` para que strings vazias sejam identificadas como NA.

```{r}
# Resposta:

```

Visualize algumas linhas de dados:

```{r}
# Resposta:

```

Use o método `str` para exibir as colunas e os tipos identificados.

```{r}
# Resposta:

```

Estamos interessados apenas nos seguintes campos: 'DATA', 'HORARIO', 'ASSUNTO', 'BAIRRO_ASS', 'SEXO', 'DATA_NASC', 'RESPOSTA_FINAL'

Crie (ou substitua) um DataFrame contendo apenas os campos acima.

```{r}
# Resposta:

```

## Localizando e tratando valores inválidos

Exiba todas as linhas com valores em branco (*NA*).

```{r}
# Resposta:

```

Exclua todas as linhas que contenham algum valor nulo (*NA*). Verifique se todas as linhas com valores em branco foram excluídas corretamente.

```{r}
# Resposta:

```

Faça com que todos os nomes de bairros fiquem em letras minúsculas (use a função `tolower()` para alterar as letras).

```{r}
# Resposta:

```

## Conversão de tipos, criação de coluna e escrita em CSV

Exiba os tipos das colunas do DataFrame.

```{r}
# Resposta:

```

Converta os campos de data para o formato de data.


```{r}
# Resposta:

```

Crie uma coluna chamada `IDADE` contendo a diferença entre o ano da reclamação e o ano de nascimento da pessoa.

**Dica:** Para extrair o ano de uma coluna do tipo data, use a função `year(coluna)` da biblioteca lubridate. Por exemplo: `year(DATA)`.

```{r}
# Resposta:

```

Verifique se as colunas foram corretamente convertidas para os tipos corretos.

```{r}
# Resposta:

```

Identifique outliers na coluna `IDADE`. Remova do DataFrame todas as reclamações de pessoas com idades muito baixas.

```{r}
# Resposta:

```

Salve o DataFrame em um arquivo CSV chamado `../data/2017-02-01_156_-_Base_de_Dados_sample-limpo.csv`. 

```{r}
# Resposta:
```

