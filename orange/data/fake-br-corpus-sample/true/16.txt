Conselho de Ãtica do PMDB decide expulsar a senadora KÃ¡tia Abreu

Presidente do partido, senador Romero JucÃ¡, afirmou que adotarÃ¡ decisÃ£o de imediato. Senadora disse ter sido expulsa por contrariar o governo. Ela tem prazo de dez dias para recorrer da decisÃ£o.

O Conselho de Ãtica do PMDB decidiu nesta quinta (23), por unanimidade, expulsar a senadora KÃ¡tia Abreu (TO) do partido e cancelar a filiaÃ§Ã£o partidÃ¡ria da senadora.

O senador Romero JucÃ¡, presidente nacional do partido, disse que acatarÃ¡ de imediato a decisÃ£o do Conselho de Ãtica. Ele disse que a medida "demonstra nova fase de posicionamento do partido".

Em nota, KÃ¡tia Abreu afirmou que foi expulsa por adotar posiÃ§Ãµes contrÃ¡rias Ã s do governo. "Fui expulsa exatamente por nÃ£o ter feito concessÃ£o Ã  Ã©tica na polÃ­tica. Fui expulsa por defender posiÃ§Ãµes que desagradam ao governo. Fui expulsa pois ousei dizer nÃ£o a cargos, privilÃ©gios ou regalias do poderâ, afirmou a senadora.

A expulsÃ£o de KÃ¡tia Abreu do PMDB Ã© assunto desde setembro do ano passado, depois que ela votou contra o impeachment da ex-presidente Dilma Rousseff. A senadora foi ministra da Agricultura no governo Dilma e tem feito duras crÃ­ticas ao governo do presidente Michel Temer, tambÃ©m filiado ao PMDB â ela se posicionou, por exemplo, contra a reforma trabalhista e a reforma da PrevidÃªncia, consideradas prioritÃ¡rias pelo governo federal.

O Conselho de Ãtica entendeu que houve falta de decoro e insurgÃªncia contra o partido. AlÃ©m disso, KÃ¡tia Abreu estaria denegrindo a imagem integrantes da legenda, segundo informaÃ§Ãµes da assessoria do partido.

Em agosto, a ComissÃ£o de Ãtica do partido jÃ¡ tinha decidido pelo afastamento da senadora. Ela Ã© acusada de ter violado o CÃ³digo de Ãtica e Fidelidade PartidÃ¡ria e o Estatuto da sigla.

KÃ¡tia Abreu nega irregularidades e afirma que o PMDB nÃ£o propÃ´s nenhum tipo de puniÃ§Ã£o a filiados condenados por crimes graves, como corrupÃ§Ã£o e formaÃ§Ã£o de quadrilha.