Bolsonaro Ã© um liberal completo, diz presidente do PSL

Partido de pouca expressÃ£o nacional, o PSL vive momentos de destaque inÃ©dito em sua trajetÃ³ria de duas dÃ©cadas.

No dia 5, a sigla anunciou que "receberia Jair Bolsonaro e sua prÃ©-candidatura Ã  PresidÃªncia da RepÃºblica".

A palavra "filiaÃ§Ã£o" nÃ£o consta no termo divulgado Ã  imprensa, mas o presidente do PSL, o deputado federal Luciano Bivar (PE), diz Ã  Folha que, "tÃ£o certo como dois e dois sÃ£o quatro", o prÃ©-candidato, hoje no PSC, migrarÃ¡ para seu partido em marÃ§o.

O acordo com o segundo colocado na corrida presidencial atÃ© o momento, segundo pesquisas do Datafolha, lanÃ§ou luz sobre a sigla nanica, mas tambÃ©m provocou uma cisÃ£o: o movimento Livres rompeu uma uniÃ£o de quase dois anos com o PSL, que tinha como objetivo refundar o partido com nova lideranÃ§a.

Para os lÃ­deres do grupo, Bolsonaro representa o extremo oposto dos valores liberais, tanto em termos econÃ´micos quanto comportamentais, que deveriam nortear a nova fase do partido.

Bivar, entretanto, diz que as crÃ­ticas sÃ£o preconceituosas. "Foi um absurdo. A maior parte do partido foi favorÃ¡vel Ã  uniÃ£o. Ter preconceito contra uma pessoa Ã© fundamentalismo. Eu lamento."

Bolsonaro representa, diz Bivar, os princÃ­pios fundadores do partido: a economia de mercado, as liberdades individuais, a livre expressÃ£o de ideias, a autonomia das instituiÃ§Ãµes, o Estado de Direito.

"NÃ£o vejo nada no comportamento dele que contradiga a ideologia liberal, na Ã¡rea econÃ´mica ou social. Ã um liberal ao aceitar que cada um faÃ§a suas escolhas. Jamais farÃ­amos como o PT, por exemplo, que apoia Cuba, onde homossexuais iam para o paredÃ£o."

Bivar minimiza declaraÃ§Ãµes do prÃ©-candidato que contrariam essa imagem, como o elogio coronel Carlos Alberto Brilhante Ustra, um dos principais sÃ­mbolos da repressÃ£o durante a ditadura militar, durante votaÃ§Ã£o do processo de impeachment da ex-presidente Dilma Rousseff (PT) na CÃ¢mara, em 2016.

"Pela famÃ­lia e inocÃªncia das crianÃ§as que o PT nunca respeitou, contra o comunismo, o Foro de SÃ£o Paulo e em memÃ³ria do coronel Brilhante Ustra, o meu voto Ã© sim", declarou na ocasiÃ£o.

"NÃ£o se deve rotular Bolsonaro por isso", diz Bivar. "NÃ£o foi um erro. Cada um tem suas convicÃ§Ãµes. NÃ£o sei se o Ustra foi um torturador ou nÃ£o. Muitas vezes criam-se factoides e toma-se aquilo como verdadeiro."

Segundo o relatÃ³rio final da ComissÃ£o Nacional da Verdade, na gestÃ£o de Ustra o DOI-CODI de SÃ£o Paulo foi responsÃ¡vel pela morte ou desaparecimento de ao menos 45 presos polÃ­ticos.

Bivar tambÃ©m nÃ£o vÃª contradiÃ§Ã£o em um suposto liberal fazer elogios a uma ditadura.

"Ã preciso entender que naquele perÃ­odo [na ditadura] estÃ¡vamos diante de uma situaÃ§Ã£o muito delicada. A ameaÃ§a comunista era muito grande. EntÃ£o o povo clamou pelos militares", diz.

"Eu faÃ§o alguns elogios ao regime militar tambÃ©m. Toda a infraestrutura do Brasil foi feita no governo militar. Foi um regime de transiÃ§Ã£o, e os prÃ³prios militares devolveram o poder de forma absolutamente espontÃ¢nea. Reconhecer isso nÃ£o torna ninguÃ©m menos liberal."

Sobre o fato de Bolsonaro ser rÃ©u Supremo Tribunal Federal, sob acusaÃ§Ã£o de incitaÃ§Ã£o ao estupro, avalia que as falas dele tiveram seu sentido deturpado. Em discurso no plenÃ¡rio da CÃ¢mara, em 2014, Bolsonaro disse que sÃ³ nÃ£o estupraria a colega Maria do RosÃ¡rio (PT-RS), ex-ministra de Direitos Humanos, porque ela "nÃ£o merecia".

"Ele nunca falou que iria estuprar a deputada. Nunca fez nenhuma incitaÃ§Ã£o. Agora, se querem deturpar, paciÃªncia. NÃ£o faz sentido nenhum o acusarem."

"AlÃ©m disso, como Ã© que pode um cara ser processado por uma declaraÃ§Ã£o dada na tribuna do Congresso, um direito legÃ­timo e constitucional de vocÃª expressar suas opiniÃµes? Essas coisas nÃ£o podem existir no Estado de Direito."

Segundo Bivar, Bolsonaro foi atraÃ­do pelo discurso transparente e probo do PSL.

"Se alguÃ©m no PSL pensa que vai se locupletar no futuro com uma eventual vitÃ³ria de Bolsonaro, Ã© melhor procurar outro partido. NinguÃ©m se juntarÃ¡ a nÃ³s por achar que poderÃ¡ participar do poder por participar."

Ele define o presidenciÃ¡vel como um homem bem intencionado e puro, que "fala o que sente, o que vem da alma". "Essa Ã© a grande diferenÃ§a. NÃ£o Ã© um polÃ­tico que fica em cima do muro. A imprensa terÃ¡ uma surpresa muito grande e positiva com relaÃ§Ã£o ao comportamento que vamos apresentar."

*POLÃMICA NO FUTEBOL*

Luciano Bivar foi eleito para a CÃ¢mara Federal pela primeira vez em 1998. Iniciou seu segundo mandato, como suplente, em julho do ano passado. Candidato a presidente em 2006, sempre pelo PSL, recebeu 0,06% dos votos.

AlÃ©m da polÃ­tica, tem longa atuaÃ§Ã£o como dirigente esportivo. Foi quatro vezes presidente do Sport Club do Recife.

Em maio de 2013, foi suspenso do posto por 180 dias, pelo Superior Tribunal de JustiÃ§a Desportiva, apÃ³s ter declarado que pagou a um lobista para "empurrar" um jogador do Sport, Leomar, para a seleÃ§Ã£o brasileira em 2001.

Na ocasiÃ£o, Bivar disse que a decisÃ£o foi absurda. "Eu fiz marketing. EntÃ£o a 9ine, do Ronaldinho, tambÃ©m deveria ser suspensa. Ã uma empresa de marketing", declarou.

Em dezembro de 2013 se afastou do comando do clube para se dedicar Ã  campanha eleitoral do ano seguinte.

Na eleiÃ§Ã£o deste ano pretende se reeleger deputado federal. 